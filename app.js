const express = require('express')
const bodyParser = require('body-parser')
const log = function (/**/) { console.log.apply(console, arguments) }
const { WebClient } = require('@slack/web-api');

const settings = {
    title: 'Control Hub Integration',
    port: 3000,
   
    // See https://github.com/slackapi/node-slack-sdk
    slack: {
        token: process.env.SLACK_TOKEN, // An access token (from your Slack app or custom integration - xoxp, xoxb)
        conversationId: process.env.SLACK_CONVERSATION_ID, // This argument can be a channel ID, a DM ID, a MPDM ID, or a group ID
    }
}

const slack = new WebClient(settings.slack.token);

async function processWebhookUpdate (req, res) {
    let webhook = req.body

    log('Processing a webhook update from Control Hub', webhook)

    log(`Got '${webhook.action}'`)
    if( webhook.stateChanged ) {
        const reference = webhook.externalId || webhook.referenceId || webhook.eventId || 'ID is missing' // Get some kind of human recognisable Id from the metadata
        const text = `Event '${reference}' changed to '${webhook.state}'.`
        try{
            const response = await slack.chat.postMessage({ channel: settings.slack.conversationId, text });
            log(`Message sent: ${response.ts}`)
        }
        catch (err) {
            log("An error occured posting to Slack", err, JSON.stringify(err.data))
        }        
    }

    log('Done processing.')
    res.sendStatus(200)
}

const app = express()
app.use(bodyParser.json())

const handler = express.Router()
handler.post('/inbound', processWebhookUpdate)

const healthcheck = express.Router()
healthcheck.get('/', (_, res) => res.sendStatus(200))

app.use('/api', handler)
app.use('/status', healthcheck)
app.listen(settings.port, () => {
    log(`${settings.title} listening on port ${settings.port}`)
})